<?php
namespace Rasel\BITM\SEIP106854\Book;

use \Rasel\BITM\SEIP106854\Utility\Utility;

class Book {
    
    public $id = "";
    public $title = "";
    public $author = "";
    //public $created = "";
    ///public $modified = "";
    // public $created_by = "";
    // public $modified_by = "";
       public $deleted_at = null; //soft delete
    
    public function __construct($data = false){
        
        if( is_array($data) && array_key_exists('id', $data) && !empty($data['id'])){
            $this->id = $data['id'];
        }
        
        $this->title = $data['title'];
        $this->author = $data['author'];
    }
    
    public function show($id=false){
        $conn = mysql_connect("localhost","root","") or die("Cannot connect database.");
        $lnk = mysql_select_db("atomicproject") or die("Cannot select database.");

        $query = "SELECT * FROM `books` WHERE id =".$id;
        $result = mysql_query($query);
        
        $row = mysql_fetch_assoc($result);
        
        return $row;  
    }
    
    public function index(){
        
        $books = array();
        
        $conn = mysql_connect("localhost","root","") or die("Cannot connect database.");
        $lnk = mysql_select_db("atomicproject") or die("Cannot select database.");

        $query = "SELECT * FROM `books` WHERE deleted_at IS NULL";
        $result = mysql_query($query);
        
        while($row = mysql_fetch_assoc($result)){
            $books[] = $row;
        }
        return $books;
    }
    
     public function trashed(){
          $conn = mysql_connect("localhost","root","") or die("Cannot connect database.");
        $lnk = mysql_select_db("atomicproject") or die("Cannot select database.");
        
        $books = array();
        $query = "SELECT * FROM `books` WHERE deleted_at IS NOT NULL";
       // Utility::dd($query);
        $result = mysql_query($query);
        
  
        while($row= mysql_fetch_assoc($result)){
            $books[] = $row;
        }
        return $books;
    }
    public function trash($id = null){
         $conn = mysql_connect("localhost","root","") or die("Cannot connect database.");
        $lnk = mysql_select_db("atomicproject") or die("Cannot select database.");
       
        if(is_null($id)){
            Utility::message('No id avaiable. Sorry !');
            return Utility::redirect('index.php');
        }
        
        $this->id = $id;
        $this->deleted_at = time();
        
        $query = "UPDATE `atomicproject`.`books` SET `deleted_at` = '".$this->deleted_at."' WHERE `books`.`id` = ".$this->id;

       //Utility::dd($query);
        $result = mysql_query($query);
               
        if($result){
            Utility::message("Book title is trashed successfully.");
        }else{
            Utility::message(" Cannot trash.");
        }
        
        Utility::redirect('index.php');
    }
    public function store(){
       
        $conn = mysql_connect("localhost","root","") or die("Cannot connect database.");
        $lnk = mysql_select_db("atomicproject") or die("Cannot select database.");
        
        $query = "INSERT INTO `atomicproject`.`books` ( `title`,`author`) VALUES ( '".$this->title."','".$this->author."')";
        //echo $query;
        //"INSERT INTO `atomicproject`.`books` (`title`, `author`) VALUES ('".$this->title."', '".$title->author."')";
        //echo $query;
        //die();
        $result = mysql_query($query);
               
        if($result){
            Utility::message("Book title is added successfully.");
        }else{
            Utility::message("There is an error while saving data. Please try again later.");
        }
        
        Utility::redirect('index.php');
    }
    
    
    public function delete($id = null){
       
        if(is_null($id)){
            Utility::message('No id avaiable. Sorry !');
            return Utility::redirect('index.php');
        }
        
        $conn = mysql_connect("localhost","root","") or die("Cannot connect database.");
        $lnk = mysql_select_db("atomicproject") or die("Cannot select database.");

        $query = "DELETE FROM `atomicproject`.`books` WHERE `books`.`id` = ".$id;
        $result = mysql_query($query);
               
        if($result){
            Utility::message("Book title is deleted successfully.");
        }else{
            Utility::message(" Cannot delete.");
        }
        
        Utility::redirect('index.php');
    }
     public function recover($id = null){
          $conn = mysql_connect("localhost","root","") or die("Cannot connect database.");
        $lnk = mysql_select_db("atomicproject") or die("Cannot select database.");
       
        if(is_null($id)){
            Utility::message('No id avaiable. Sorry !');
            return Utility::redirect('index.php');
        }
        
        $this->id = $id;
        


        $query = "UPDATE `atomicproject`.`books` SET `deleted_at` = NULL WHERE `books`.`id` = ".$this->id;

        //Utility::dd( $query);
        $result = mysql_query($query);
               
        if($result){
            Utility::message("Book title is recovered successfully.");
        }else{
            Utility::message(" Cannot recover.");
        }
        
        Utility::redirect('index.php');
    }
    
    public function recovermultiple($ids = array()){
         $conn = mysql_connect("localhost","root","") or die("Cannot connect database.");
        $lnk = mysql_select_db("atomicproject") or die("Cannot select database.");
       
        if(is_array($ids) && count($ids) > 0){
            
            //Utility::dd($ids);
            $_ids = implode(',',$ids);



            $query = "UPDATE `atomicproject`.`books` SET `deleted_at` = NULL WHERE `books`.`id` IN($_ids) ";

            //Utility::dd( $query);
            $result = mysql_query($query);

            if($result){
                Utility::message("Book title is recovered successfully.");
            }else{
                Utility::message(" Cannot recover.");
            }

            Utility::redirect('index.php');
        }else{
            Utility::message('No id avaiable. Sorry !');
            return Utility::redirect('index.php');
        }
        

    }
    public function deletemultiple($ids = array()){
         $conn = mysql_connect("localhost","root","") or die("Cannot connect database.");
        $lnk = mysql_select_db("atomicproject") or die("Cannot select database.");
       
            if(is_array($ids) && count($ids) > 0){

                 //Utility::dd($ids);
                 $_ids = implode(',',$ids);



                 $query = "DELETE FROM `atomicproject`.`books` WHERE `books`.`id` IN($_ids) ";

                 //Utility::dd( $query);
                 $result = mysql_query($query);

                 if($result){
                     Utility::message("Book titles are deleted successfully.");
                 }else{
                     Utility::message(" Cannot delete.");
                 }

                 Utility::redirect('index.php');
             }else{
                 Utility::message('No id avaiable. Sorry !');
                 return Utility::redirect('index.php');
             }
    }
    
        public function update(){
            
        $conn = mysql_connect("localhost","root","") or die("Cannot connect database.");
        $lnk = mysql_select_db("atomicproject") or die("Cannot select database.");
        
        $query = "UPDATE `atomicproject`.`books` SET `title` = '".$this->title."', `author` = '".$this->author."' WHERE `books`.`id` = ".$this->id;

        $result = mysql_query($query);
               
        if($result){
            Utility::message("Book title is edited successfully.");
        }else{
            Utility::message("There is an error while saving data. Please try again later.");
        }
        
        Utility::redirect('index.php');
    }
    
    
}
