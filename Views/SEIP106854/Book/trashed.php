<?php
include_once($_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR.'AtomicProject1'.DIRECTORY_SEPARATOR.'Views'.DIRECTORY_SEPARATOR.'startup.php');
     use \Rasel\BITM\SEIP106854\Book\Book;
    use  \Rasel\BITM\SEIP106854\Utility\Utility;
    $book = new Book();
    $books = $book->trashed();
    
    
?>
    
<!DOCTYPE html>
<html>
    <head>
        <title>Trashed</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
          <link rel="stylesheet" href="../../../style.css">
           <link href="../../../bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <style>
            #utility{
                float:right;
                width:60%;
            }
            #message{
                background-color:green;
            }

        </style>
    </head>
        <section>
            <?php
            include_once "../../../page/header.php";
            ?>
        </section>
    <body>
        <div class="container">
        <h1>Book Title</h1>
        
        <div id="message">
            <?php echo Utility::message(); ?>            
        </div>
        
        <form action="recovermultiple.php" method="post">
            <div class=" glyphicon left"><button type="submit">Recover</button></div>
            <div class=" glyphicon glyphicon-erase right"><button type="button" id="deleteAll">Delete</button></div>
            <table class="table table-bordered">
            <thead>
                <tr>
                    <th><input type="checkbox" name="markall" id="markall" >Select All</th>
                    <th>Sl.</th>
                    <th>ID</th>
                    <th>Book Title &dArr;</th>
                     <th>Author &dArr;</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
               <?php
               if(count($books) > 0){
               
               $slno =1;
               foreach($books as $book){
               ?>
                <tr>
                    <td><input type="checkbox" class="mark" name="mark[]" value="<?php echo $book['id'];?>"></td>
                    <td><?php echo $slno;?></td>
                    <td><?php echo $book['id'];?></td>
                    <td><a href="show.php?id=<?php echo $book['id'];?>"><?php echo $book['title'];?></a></td>
                    <td><?php echo $book['author'];?></td>
                    <td>
                        <a href="recover.php?id=<?php echo $book['id'];?>">Recover</a>
                       
                        
                        | <a href="delete.php?id=<?php echo $book['id'];?>" class="delete">Delete</a>
                        
                         </td>
                </tr>
            <?php
           $slno++;
            }
            
               }else{
            ?>
                <tr>
                    <td colspan="6">No record is available.</td>
                </tr> 
                <?php
               }
                ?>
            </tbody>
        </table>
        </form>
    
        <div><span> prev  1 | 2 | 3 next </span></div>
        
            <nav>
            <li><a href="index.php">Go to list</a></li>
        </nav>
        <script src="https://code.jquery.com/jquery-2.1.4.min.js" type="text/javascript" ></script>
        <script>

    
    $(document).ready(function(){
        
        $('.delete').bind('click',function(e){
        var deleteItem = confirm("Are you sure you want to delete?");
            if(!deleteItem){
                //return false; 
                e.preventDefault();
            }
        });     
        $('#message').hide(5000);
        
        $('#markall').bind('click', function(){
            if($('#markall').is(':checked')){
                $('.mark').each(function() { //loop through each checkbox
                    this.checked = true;  //select all checkboxes with class "mark"               
                });
            }else{
                $('.mark').each(function() { //loop through each checkbox
                    this.checked = false;  //select all checkboxes with class "mark"               
                });
            }
        });
        
         $('#deleteAll').bind('click',function(e){
            
            var startDeleteProcess = false;
            $('.mark').each(function() { //loop through each checkbox
                  if($(this).is(':checked')){
                      startDeleteProcess = true;                
                  }       
            });
           
            if(startDeleteProcess){
                var deleteItem = confirm("Are you sure you want to delete all Items??");
                    if(deleteItem){
                        document.forms[0].action = 'deletemultiple.php';
                        document.forms[0].submit();
                    } 
            }else{
                alert("Please select an item first");
            }
            

        }); 
        
    });
    
    

    
    
        </script>
        </div>
    </body>
    <section>
            <?php
            include_once "../../../page/footer.php";
            ?>
        </section>
</html>

