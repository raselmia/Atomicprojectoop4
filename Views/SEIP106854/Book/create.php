<!DOCTYPE html>
<html>
    <head>
        <title>Create</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
         <link rel="stylesheet" href="../../../style.css">
           <link href="../../../bootstrap/css/bootstrap.min.css" rel="stylesheet">
    </head>
     <?php
    include_once "../../../page/header.php";
    ?>
    <body>
        <div class="container">
        <form action="store.php" method="post">
            <fieldset>
                <legend>Add Book Title</legend>
                
                <div>
                    <label>Enter Book Title:</label>
                    <input autofocus="autofocus" 
                    
                           placeholder="Enter the title of your favorite book" 
                           type="text" 
                           name="title"
                     
                           required="required"
                      
                           /><br><br>
                 </div>
                <div>
                    <label>Enter Author:</label>
                    <input placeholder="Enter author name" 
                           type="text" 
                           name="author"
                           required="required"
                      
                           /><br><br>
                </div>
                <button class="btn btn-primary" type="submit">Save</button>
                <button class="btn btn-success" type="submit">Save & Add Again</button>
<!--                <input type="submit" value="Save" />-->
                <input class="btn btn-info" type="reset" value="Reset" />
            </fieldset>
        </form> 
        <nav>
            <li class="btn btn-success"><a href="index.php">Go to List</a></li>
            <li class="btn btn-success right"><a href="javascript:history.go(-1)">Back</a></li>
        </nav>
        </div>
    </body>
      <?php include_once "../../../page/footer.php"
    ?>
</html>


