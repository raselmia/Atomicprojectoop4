

<?php
include_once($_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR.'AtomicProject1'.DIRECTORY_SEPARATOR.'Views'.DIRECTORY_SEPARATOR.'startup.php');

use \Rasel\BITM\SEIP106854\ProfilePicture\Profile;
use  \Rasel\BITM\SEIP106854\Utility\Utility;

$book = new Profile();
$books = $book->index();
?>



<html>
    <head>
        <title>Profile Picture</title>
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="../../../style.css">
           <link href="../../../bootstrap/css/bootstrap.min.css" rel="stylesheet">
    </head>
     <?php 



       include_once "../../../page/header.php";




    ?>
    <body>

        <div class="container">
            <div class="col-md-8">

            </div>
            <div class="col-md-4">

                <form class="form-inline">
                    <input type="text" class="form-control"  placeholder="Search">
                    <button type="submit" class="btn btn-default ">Search</button>
                </form>
            </div>
            <div class="container ">
                <div class="jumbotron">
                    <a href="create.php"><button class="btn btn-success" style="float:left;margin-bottom:10px">Add Profile Photo</button></a><a href="#"><button class="btn btn-success" style="float: right;">Dowland as PDF | XL </button></a>
                    <table class="table table-bordered table-responsive">
                        <thead>
                        <td>
                            SL
                        </td>
                        <td>Name</td>
                        <td>Image</td>
                        <td>Action</td>
                        </thead>
                        <tbody>
                            <?php
                            $slno = 1;
                            foreach ($books as $book) {
                                ?>
                                <tr>
                                    <td><?php echo $slno; ?></td>
                                    <td><?php echo $book->name; ?></td>
                                    <td><?php echo"<img src='images/$book->photo' alt='Mountain View'style='width:100px;height:100px;'>"?></td>

                                    <td>
                                        <a href="show.php?id=<?php echo $book->id;?>" class="btn btn-success">View</a> 
                                        <a href="edit.php?id=<?php echo $book->id;?>" class="btn btn-warning"> Edit</a>  
                                        <a href="delete.php?id=<?php echo $book->id; ?>" class="btn btn-danger">Delete</a> 
                                        <a href="#" class="btn btn-warning">Trash/Recover</a>
                                        <a href="#" class="btn btn-default"> Email To Friend</a>


                                    </td>
                                </tr>
                                <?php
                                $slno++;
                            }
                            ?>
                        </tbody>


                    </table>
                    <div>
                        <ul class="pagination">
                            <li><a href="#">1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#">4</a></li>
                            <li><a href="#">5</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>