<?php
    include_once($_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR.'AtomicProject1'.DIRECTORY_SEPARATOR.'Views'.DIRECTORY_SEPARATOR.'startup.php');
    use Rasel\Bitm\SEIP106854\TextSummary\Summary;
     use \Rasel\Bitm\SEIP106854\Utility\Utility;
    $summary = new Summary();
    $summarys = $summary->index();
    
    
    
?>

<!DOCTYPE html>
<html>
    <head>
        <title>Summary</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="../../../style.css">
           <link href="../../../bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <style>
            #utility{
                float:right;
                
            }
        </style>
    </head>
        <?php
        include_once "../../../page/header.php";
    ?>
    <body>
     
   <div class="container">



        <h1>List Of Summary</h1>
        <div><span>Search / Filter </span> 
            <span id="utility">Download as PDF | XL  <a class="btn btn-success" href="create.php">Add New</a></span>
            <select>
                <option>10</option>
                <option>20</option>
                <option>30</option>
                <option>40</option>
                <option>50</option>
            </select>
        </div>
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>Sl.</th>
                 
                    <th>Name &dArr;</th>
                     <th>Summary list &dArr;</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
               <?php
               $slno =1;
               foreach($summarys as $summary){
               ?>
                <tr>
                    <td><?php echo $slno;?></td>
                
                    <td><a href="show.php?id=<?php echo $summary['id'];?>"><?php echo $summary['title'];?></a></td>
                       <td><?php echo $summary['summary'];?></td>
                    
                    <td> <a href="show.php?id=<?php echo $summary['id'];?>">View</a>
                        |<a href="edit.php?id=<?php echo $summary['id'];?>">Edit</a> | 
                        <a href="delete.php?id=<?php echo $summary['id'];?>" class="delete">Delete</a>
                        <form action="delete.php" method="post">
                            <input type="hidden" name ="id" value="<?php echo $summary['id'];?>">
                      
                        </form>
                        | Trash/Recover | Email to Friend
                    
                    
                    
                    </td>
                </tr>
            <?php
           $slno++;
            }
            ?>
            </tbody>
        </table>
        </div>
       
    </body>
      <ul class="pagination">
  <li><a href="#">1</a></li>
  <li class="active"><a href="#">2</a></li>
  <li><a href="#">3</a></li>
  <li><a href="#">4</a></li>
  <li><a href="#">5</a></li>
</ul>

        
        


    <?php include_once "../../../page/footer.php";
    ?>
</html>
