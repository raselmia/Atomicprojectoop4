<?php

include_once($_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR.'AtomicProject1'.DIRECTORY_SEPARATOR.'Views'.DIRECTORY_SEPARATOR.'startup.php');

use Rasel\Bitm\SEIP106854\Terms_condition\Condition;
use  \App\BITM\SEIP1020\Utility\Utility;

$condition = new Condition();
$condition = $condition->show($_GET['id']);

//Utility::dd($book);
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Condition List</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="../../../style.css">
           <link href="../../../bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <style>
            #utility{
                float:right;
                width:60%;
            }
            #message{
                background-color:green;
            }

        </style>
    </head>
     <?php 



       include_once "../../../page/header.php";




    ?>
    
    <body>
        <div class="container">
<h1>Condition List</h1>
<table class="table table-bordered">
<tr>
    <th>Id</th>
    <th>Name</th>
       <th>Condition</th>
</tr>
<tr>
    <td><?php echo $condition['id']; ?></td>
    
    
    <td><?php echo $condition['name']; ?></td>
    
 
    <td><?php echo $condition['condition']; ?></td>
</tr>
</table>
<nav>
    <li class="btn btn-success"><a href="index.php">Go to list</a></li>
</nav>

    </body>
</div>
                  <?php
include_once "../../../page/footer.php";


              ?>
</html>
