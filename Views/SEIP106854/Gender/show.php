<?php

include_once($_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR.'AtomicProject1'.DIRECTORY_SEPARATOR.'Views'.DIRECTORY_SEPARATOR.'startup.php');

use \Rasel\BITM\SEIP106854\Gender\Gender;
use  \App\BITM\SEIP1020\Utility\Utility;

$gender = new Gender();
$gender = $gender->show($_GET['id']);

//Utility::dd($book);
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Gender List</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="../../../style.css">
           <link href="../../../bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <style>
            #utility{
                float:right;
                width:60%;
            }
            #message{
                background-color:green;
            }

        </style>
    </head>
     <?php 



       include_once "../../../page/header.php";




    ?>
    
    <body>
        <div class="container">
<h1>Gender View</h1>
<table class="table table-bordered">
<tr>
    <th>Id</th>
    <th>Name</th>
    <th>Gender</th>
</tr>
<tr>
    <td><?php echo $gender['id']; ?></td>
    
    
    <td><?php echo $gender['name']; ?></td>
    
    
    <td><?php echo $gender['gender']; ?></td>
</tr>
</table>
<nav>
    <li class="btn btn-success"><a href="index.php">Go to list</a></li>
</nav>

    </body>
</div>
                  <?php
include_once "../../../page/footer.php";


              ?>
</html>