<?php

include_once($_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR.'AtomicProject1'.DIRECTORY_SEPARATOR.'Views'.DIRECTORY_SEPARATOR.'startup.php');

use \Rasel\BITM\SEIP106854\Hobby\Hobby;
use  \Rasel\BITM\SEIP106854\Utility\Utility;

$hobby = new Hobby();
$hobby= $hobby->show('id');
?>
<html>
    <head>
        <title>Hobby</title>
        <link rel="stylesheet" href="../../../bootstrap/css/bootstrap.css" />
         <link rel="stylesheet" href="../../../style.css">
    </head>
     <section>
            <?php
            include_once "../../../page/header.php";
            ?>
        </section>
    <body>

        
            <div class="container ">
                <div class="jumbotron">
                    <form action="store.php" method="POST">
                        <fieldset>
                            <label for="title">Your Name</label><br>
                            <input type="text" name="name" value="<?php echo $hobby['name']?>" required="required"> <br>
                            <label for="title">Your Hobby</label><br>
                            <input type="checkbox" name="hobby1" value="<?php echo $hobby['hoby1']?>">Cricket
                            <input type="checkbox" name="hobby2" value="<?php echo $hobby['hoby2']?>">Song
                            <input type="checkbox" name="hobby3" value="<?php echo $hobby['hoby3']?>">Movie
                            <input type="checkbox" name="hobby4" value="<?php echo $hobby['hoby4']?>">Swimming

                            <section>
                                <button type="submit" class="btn btn-success">SAVE</button>
                                <button type="submit" class="btn btn-primary">SAVE & Add Again</button>

                                <button type="reset" class="btn btn-info">RESET</button>
                            </section>
                        </fieldset> 
                    </form>

                </div>
                 <nav>
            <li class="btn btn-success"><a href="index.php">Go to List</a></li>
            <li class="btn btn-primary right"><a href="javascript:history.go(-1)">Back</a></li>
        </nav>
          
        </div>
    </body>
     <section>
            <?php
            include_once "../../../page/footer.php";
            ?>
        </section>
</html>