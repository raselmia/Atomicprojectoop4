
<?php
include_once($_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR.'AtomicProject1'.DIRECTORY_SEPARATOR.'Views'.DIRECTORY_SEPARATOR.'startup.php');
    
use Rasel\Bitm\SEIP106854\Hobby\Hobby;
use Rasel\Bitm\SEIP106854\Utility\Utility;

$hobby = new Hobby();
$hobbys = $hobby->index();
?>

<html>
    <head>
        <title>Check Box Hobby</title>
        <link rel="stylesheet" href="../../../style.css">
           <link href="../../../bootstrap/css/bootstrap.min.css" rel="stylesheet">
            <style>
            #utility{
                float:right;
               
            }
            #message{
                background-color:green;
                color: white;
            }

        </style>
           
           
           
    </head>
    
    
    
    <section>
            <?php
            include_once "../../../page/header.php";
            ?>
        </section>
    
    <body>

       
            <div class="container ">
                <div><span>Search / Filter </span> 
                    <span id="utility">Download as PDF | XL  <a class="btn btn-success" href="create.php">Add New</a></span>
            <select>
                <option>10</option>
                <option>20</option>
                <option>30</option>
                <option>40</option>
                <option>50</option>
            </select>
        </div>
                <div>
                    
                    <table class="table table-bordered table-responsive">
                       <thead>
                        <td>
                            SL
                        </td>
                        <td>Name</td>
                        <td>Hobby</td>
                        <td>Action</td>
                        </thead>
                        <tbody>
                            <?php
                            $slno = 1;
                            foreach ($hobbys as $hobby) {
                                ?>
                                <tr>
                                    <td><?php echo $slno; ?></td>
                                    <td><?php echo $hobby->name; ?></td>
                                    <td>
                               <?php echo $hobby->hoby1;echo $hobby->hoby2;echo $hobby->hoby3;echo  $hobby->hoby4; ?>
                            
                                    </td>
                                    <td>
                                         <a href="edit.php?id=<?php echo $hobby->id;?>">Edit |</a> 
                                        <a href="show.php?id=<?php echo $hobby->id;?>">View |</a>
                                       <a href="delete.php?id=<?php echo $hobby->id;?>" class="delete">Delete</a>
                                              | Trash/Recover | Email to Friend 
                                    </td>
                                </tr>
                                <?php
                                $slno++;
                            }
                            
                            ?>
                        </tbody>


                    </table>
                    <div>
                        <ul class="pagination">
                            <li><a href="#">1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#">4</a></li>
                            <li><a href="#">5</a></li>
                        </ul>
                        
                        <script src="https://code.jquery.com/jquery-2.1.4.min.js" type="text/javascript" ></script>
        <script>
           $('.delete').bind('click',function(e){
               var deleteItem = confirm("Are you sure you want to delete?");
               if(!deleteItem){
                  //return false; 
                  e.preventDefault();
               }
           }); 
    
    
    $('#message').hide(10);
        </script>
                        
                    </div>
                </div>
            </div>
      
    </body>
    <section>
            <?php
            include_once "../../../page/footer.php";
            ?>
        </section>
</html>