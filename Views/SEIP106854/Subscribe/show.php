<?php

include_once($_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR.'AtomicProject1'.DIRECTORY_SEPARATOR.'Views'.DIRECTORY_SEPARATOR.'startup.php');

use \Rasel\BITM\SEIP106854\Subscribe\Email;
use  \Rasel\BITM\SEIP106854\Utility\Utility;

$email = new Email();
$email = $email->show($_GET['id']);

//Utility::dd($book);
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Email List</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="../../../style.css">
           <link href="../../../bootstrap/css/bootstrap.min.css" rel="stylesheet">
      
    </head>
     <?php 



       include_once "../../../page/header.php";




    ?>
    
    <body>
        <div class="container">
<h1>Email Address View</h1>
<table class="table table-bordered">
    <tr>
    <th>Id</th>
    <th>Name</th>
    <th>Email Address</th>
</tr>
<tr>
    <td><?php echo $email['id']; ?></td>
    
    
    <td><?php echo $email['name']; ?></td>
    
    
    <td><?php echo $email['email_address']; ?></td>
</tr>
</table>
<nav>
    <li class="btn btn-success"><a href="index.php">Go to list</a></li>
</nav>

    </body>
</div>
                  <?php
include_once "../../../page/footer.php";


              ?>
</html>