<?php

include_once($_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR.'AtomicProject1'.DIRECTORY_SEPARATOR.'Views'.DIRECTORY_SEPARATOR.'startup.php');

use \Rasel\BITM\SEIP106854\Date\Birthday;
use  \Rasel\BITM\SEIP106854\Utility\Utility;

$birthday = new Birthday();
$birthday = $birthday->show($_GET['id']);

//Utility::dd($book);
?>
<!DOCTYPE html>
<html>
    <head>
        <title>View the Birthday </title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
          <link rel="stylesheet" href="../../../style.css">
           <link href="../../../bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <style>
            #utility{
                float:right;
                width:60%;
            }
            #message{
                background-color:green;
            }

        </style>
    </head>
     <?php 



       include_once "../../../page/header.php";




    ?>
    <body>
        <div class="container">
<h1>Birthday Details</h1>
<table class="table table-bordered">
    <tr>
    <th>Id</th>
    <th>Name</th>
     <th>Birthday Date</th>
</tr>
<tr>
    <th><?php echo $birthday['id']; ?></th>
    
    
    <th><?php echo $birthday['name']; ?></th>
    
   
    <th><?php echo $birthday['date']; ?></th>
</tr>
    </table>

<nav>
    <li class="btn btn-success"><a href="index.php">Go to list</a></li>
</nav>
</div>

    </body>
    
              <?php
include_once "../../../page/footer.php";


              ?>
</html>